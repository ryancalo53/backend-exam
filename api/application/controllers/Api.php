<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Api extends CI_Controller {

	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	// PostRequest handles HTTP POST
  private function PostRequest($payload, $uri){
		if (count($uriSgment) == 4 ){

			if ( array_key_exists("body", $payload) ){

				$this->output->set_status_header(201);	
					
					$res = array(
								"body"=> $payload->body,
								"commentable_type"=> "App\\Post",
								"commentable_id"=> 1,
								"creator_id"=> 1,
								"parent_id"=> NULL,
								"updated_at"=> date("Y-m-d H:i:s"),
								"created_at"=> date("Y-m-d H:i:s"),
								"id"=> $uriSgment,
					);
					
					echo json_encode($res);

			}else{
				$this->output->set_status_header(422);
					$res = array(
						"message"=> "The given data was invalid.",
						"errors"=> array(
							"body"=> array(
								"The body field is required."
							)
						)
					);
					echo json_encode($res);
			}

		}
	}

  // PatchRequest handles HTTP POST
	private function PatchRequest($payload, $uri){
		if (count($uriSgment) == 5 ){
			if ( array_key_exists("body", $payload) ){

				$res = array(
						"id"=>  $this->uri->segment(5),
						"title"=> null,
						"body"=> $payload->body,
						"commentable_type"=> "App\\Post",
						"commentable_id"=> 1,
						"creator_id"=> 1,
						"parent_id"=> NULL,
						"updated_at"=> date("Y-m-d H:i:s"),
						"created_at"=> date("Y-m-d H:i:s"),
					);
						return json_encode($res);

			}else{

				$this->output->set_status_header(422);
				$res = array(
					"message"=> "The given data was invalid.",
					"errors"=> array(
						"body"=> array(
							"The body field is required."
						)
					)
				);
				
				return json_encode($res);

			}
		}
	}


  // DeleteRequest handles HTTP POST
	private function DeleteRequest($payload, $uri){
		if (count($uriSgment) == 5 ){
			$res = array(
				"status"=> "record deleted successfully"
      );
				return json_encode($res);
			}
		
	}


  public function index(){
		$this->load->view('welcome_message');
	}


	public function posts($title= NULL, $category = NULL, $commentID = NULL)
	{
		$payload = json_decode(file_get_contents("php://input"));
		
    // POST REQUEST
		if ($this->input->server('REQUEST_METHOD') == 'POST' ){
			$res = $this->PostRequest($payload, $this->uri->segment_array());
			echo $res;
		}

     // PATCH REQUEST
    if ($this->input->server('REQUEST_METHOD') == 'PATCH' && $uriSgment == 5){
			$res = $this->PatchRequest($payload, $this->uri->segment_array());
			echo $res;
		}


		  // DELETE REQUEST 
		  if ($this->input->server('REQUEST_METHOD') == 'DELETE' && $uriSgment == 5){
				$res = $this->DeleteRequest($payload, $this->uri->segment_array());
				echo $res;        
		  }







		  }
		
	}

